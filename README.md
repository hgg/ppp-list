# ppp-list 

Documentação de Parcerias público-privadas realizadas pelo Governo Português. 

## Link

https://bertolo1988.github.io/ppp-list/

## Motivação 

Este repositório foi criado no seguimento de uma [thread no /r/portugal](https://www.reddit.com/r/portugal/comments/83mh0q/contratos_das_ppp/) criada pelo [/u/hgg](https://www.reddit.com/user/hgg) 

## Contribuir

Contribuições com mais informação relevante/contratos, correção de errors ou com qualquer outra melhoria são bem vindas. Senão se sentirem capazes de fazer um [pull request](https://help.github.com/articles/about-pull-requests/) abram um [issue aqui.](https://github.com/bertolo1988/ppp-list/issues)


## Contratos

* [Lusoponte](./contratos/Lusoponte.pdf)           

* [Lusoponte_Acordo_2000](./contratos/Lusoponte_Acordo_2000.pdf)                        

* [Norte](./contratos/Norte.pdf)

* [Norte_Acordo_Aditamento_2010](./contratos/Norte_Acordo_Aditamento_2010.pdf)          

* [Oeste](./contratos/Oeste.pdf)

* [LitoralCentro](./contratos/LitoralCentro.pdf)                                        

* [BeiraInterior](./contratos/BeiraInterior.pdf)

* [CostaPrata](./contratos/CostaPrata.pdf)        

* [CostaPrata_Acordo_Aditamento_2010](./contratos/CostaPrata_Acordo_Aditamento_2010.pdf)

* [Algarve](./contratos/Algarve.pdf)                                     

* [InteriorNorte](./contratos/InteriorNorte.pdf)      

* [BeiraLitoralAlta](./contratos/BeiraLitoralAlta.pdf)

* [BLA_Acordo_Aditamento_Contrato_2010](./contratos/BLA_Acordo_Aditamento_Contrato_2010.pdf)

* [NorteLitoral](./contratos/NorteLitoral.pdf)

* [NL_Acordo_Quadro_Final_2010](./contratos/NL_Acordo_Quadro_Final_2010.pdf)        

* [GrandePorto](./contratos/GrandePorto.pdf)

* [GrandePorto_Aditamento_2007](./contratos/GrandePorto_Aditamento_2007.pdf)    

* [GrandePorto_Acordo_Aditamento_Contrato_2010](./contratos/GrandePorto_Acordo_Aditamento_Contrato_2010.pdf)

* [GrandeLisboa](./contratos/GrandeLisboa.pdf)    

* [GL_Acordo_Aditamento_Contrato_2010](./contratos/GL_Acordo_Aditamento_Contrato_2010.pdf)

* [DouroLitoral](./contratos/DouroLitoral.pdf) 

* [Transmontana](./contratos/Transmontana.pdf)

* [DouroInterior](http://www.dgtf.pt:80/ResourcesUser/PPP/Documentos/contratos/DouroInterior.pdf)                                        
* [TunelMarao](./contratos/TunelMarao.pdf)                                              

* [BaixoAlentejo](./contratos/BaixoAlentejo.pdf)                                        

* [BaixoTejo](./contratos/BaixoTejo.pdf)                                                

* [LitoralOeste](./contratos/LitoralOeste.pdf)                                          

* [AlgarveLitoral](./contratos/AlgarveLitoral.pdf)                                      

* [PinhalInterior](./contratos/PinhalInterior.pdf)                                      

* [MetroSulTejo](./contratos/MetroSulTejo.pdf)                                          

* [EixoNorteSul](./contratos/EixoNorteSul.pdf)                                          

* [EixoNorteSul_Acordo_2010](./contratos/EixoNorteSul_Acordo_2010.pdf)                  

* [PoceiraoCaia](./contratos/PoceiraoCaia.pdf)                                          

* [PoceiraoCaia_reforma](./contratos/PoceiraoCaia_reforma.pdf)                          

* [CASNS](./contratos/CASNS.pdf)                                                        

* [CASNS_Adenda_2009](./contratos/CASNS_Adenda_2009.pdf)                                

* [CASNS_Adenda_2011](./contratos/CASNS_Adenda_2011.pdf)                                

* [ReabilitacaoSul](./contratos/ReabilitacaoSul.pdf)                                    

* [HBraga](./contratos/HBraga.pdf)                                                      

* [HCascais](./contratos/HCascais.pdf)                                                  

* [HLoures](./contratos/HLoures.pdf)                                                    

* [HVFX](./contratos/HVFX.pdf)                                                          

* [SIRESP](./contratos/SIRESP.pdf)
